public class Pedestrian {
    private double x, y, speed, angle, deltaAngle;
    private double x_tPlusDelta, y_tPlusDelta;
    private Comuna comuna;
    public PedestrianView view;
    public Pedestrian (Comuna comuna, double speed, double deltaAngle){
        angle = Math.random()*2*Math.PI;
        double width = SimulatorConfig.WIDTH;
        double largo = SimulatorConfig.LENGTH;
        this.x=Math.random()*width;
        this.y=Math.random()*largo;
        this.comuna = comuna;
        this.speed = speed;
        this.deltaAngle = deltaAngle;
        this.x_tPlusDelta = this.x;
        this.y_tPlusDelta = this.y;
        view = new PedestrianView(comuna,this);
    }
    public double getX(){return this.x;}
    public double getY(){return this.y;}
    public void computeNextState(double delta_t) {
        double r=Math.random();

        angle+= Math.random()<0.5 ? -Math.random()*deltaAngle:Math.random()*deltaAngle;
        x_tPlusDelta=x+speed*delta_t*Math.cos(angle);
        y_tPlusDelta = y+speed*delta_t*Math.sin(angle);

        if(x_tPlusDelta < 0){   // rebound logic: pared izquierda
            angle = Math.PI - angle;
            x_tPlusDelta = -x_tPlusDelta;
        }
        else if( x_tPlusDelta > comuna.getWidth()){ // rebound logic: pared derecha
            x_tPlusDelta = 2* comuna.getWidth()-x_tPlusDelta;
            angle = Math.PI - angle;
        }
        if(y_tPlusDelta < 0){   // rebound logic: pared inferior
            y_tPlusDelta = -y_tPlusDelta;
            angle = -angle;
        }
        else if( y_tPlusDelta > comuna.getHeight()){  // rebound logic: pared superior
            y_tPlusDelta = 2* comuna.getHeight()-y_tPlusDelta;
            angle = -angle;
        }
    }//se encarga de predecir la próxima posicion del individuo
    public void updateState(){
        x=x_tPlusDelta;
        y=y_tPlusDelta;
    }//actualiza los datos actuales por los del tipo tPlusDelta
    public double getX_tPlusDelta(){return this.x_tPlusDelta;}
    public double getY_tPlusDelta(){return this.y_tPlusDelta;}
}