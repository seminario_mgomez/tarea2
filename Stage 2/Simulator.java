import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.input.KeyCode;
import javafx.scene.media.AudioClip;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;
import javafx.scene.input.KeyEvent;

public class Simulator {
    private Timeline animation;
    private Comuna comuna;
    private double simulationSamplingTime;
    private double simulationTime;  // it goes along with real time, faster or slower than real time
    private double delta_t;   // precision of discrete simulation time
    public static AudioClip beep;


    /**
     * @param framePerSecond frequency of new views on screen
     * @param simulationTime2realTimeRate how faster the simulation runs relative to real time
     */
    public Simulator (double framePerSecond, double simulationTime2realTimeRate, Comuna comuna){
        this.comuna = comuna;
        double viewRefreshPeriod = 1 / framePerSecond; // in [ms] real time used to display
        beep = new AudioClip(getClass().getResource("beep.mp3").toExternalForm());
        simulationSamplingTime = viewRefreshPeriod *simulationTime2realTimeRate;
        delta_t = SimulatorConfig.DELTA_T;
        simulationTime = 0;
        animation = new Timeline(new KeyFrame(Duration.millis(viewRefreshPeriod*1000), e->takeAction()));
        animation.setCycleCount(Timeline.INDEFINITE);
    }
    private void takeAction() {
        double nextStop=simulationTime+simulationSamplingTime;
        for(; simulationTime<nextStop; simulationTime+=delta_t) {
            comuna.computeNextState(); // compute its next state based on current global state
            comuna.updateGraph(simulationTime);
            comuna.updateState();            // update its state
            comuna.updateView();
        }}

    public void start(){
        comuna.poblacionRestart();
        animation.play();
        SimulatorMenuBar.setMenu.setDisable(true);
        comuna.getView().setOnKeyPressed( e->keyHandle(e));
    }
    private void keyHandle (KeyEvent e) {
        switch (e.getCode()){
            case RIGHT:
                speedup();
                break;
            case LEFT:
                slowdown();
                break;
        }
    }
    public void stop(){
        animation.stop();
        SimulatorMenuBar.setMenu.setDisable(false);
    }
    public void prueba(){
        Stage stage = new Stage();
        stage.setTitle("Second Stage");
        //Cajas
        VBox vbox = new VBox(5);
        HBox N_I_Box = new HBox(5);
        HBox Speed_Deltat_Deltatheta_P_Box = new HBox(5);
        HBox boton_box = new HBox();
        //Formulario
        Spinner<Integer> N = new Spinner<Integer>();
        SpinnerValueFactory<Integer> valueFactory_N = new SpinnerValueFactory.IntegerSpinnerValueFactory(1,100,1);
        N.setValueFactory(valueFactory_N);
        Spinner<Integer> I = new Spinner<Integer>();
        SpinnerValueFactory<Integer> valueFactory_I = new SpinnerValueFactory.IntegerSpinnerValueFactory(1,100,1);
        I.setValueFactory(valueFactory_I);
        TextField Speed = new TextField();
        TextField Delta_t = new TextField();
        TextField Delta_theta = new TextField();
        TextField P = new TextField();
        Button boton = new Button("Aplicar cambios");
        Speed.setPromptText("Speed");
        Delta_t.setPromptText("Delta_t");
        Delta_theta.setPromptText("Delta_theta");
        P.setPromptText("P de infectarse");
        //Agregar eventos
        boton.setOnAction(e->{
            try {
                SimulatorConfig.SPEED = Double.parseDouble(Speed.getText());
                SimulatorConfig.DELTA_T = Double.parseDouble(Delta_t.getText());
                SimulatorConfig.DELTA_THETA = Double.parseDouble(Delta_theta.getText());
                SimulatorConfig.P0 = Double.parseDouble(P.getText());
            }catch (NumberFormatException error){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setTitle("Error");
                alert.setContentText("Parametros vacios");
                alert.showAndWait();
            }
        });
        //Configuracion de cajas
        vbox.setAlignment(Pos.CENTER);
        N_I_Box.setAlignment(Pos.CENTER);
        Speed_Deltat_Deltatheta_P_Box.setAlignment(Pos.CENTER);
        boton_box.setAlignment(Pos.CENTER);
        //Insertar formularios en cajas
        N_I_Box.getChildren().addAll(new Label("set N"),N,new Label("set I"),I);
        Speed_Deltat_Deltatheta_P_Box.getChildren().addAll(Speed,Delta_t,Delta_theta,P);
        boton_box.getChildren().add(boton);
        vbox.getChildren().addAll(N_I_Box,Speed_Deltat_Deltatheta_P_Box,boton_box);
        //Construir escena
        stage.setScene(new Scene(vbox, 500, 500));
        stage.show();
    }
    public void speedup(){
        simulationSamplingTime *= 2;
    }
    public void slowdown(){
        simulationSamplingTime /= 2;
    }
}
