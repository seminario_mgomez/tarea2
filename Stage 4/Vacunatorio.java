import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Vacunatorio {
    private double semiLenght;
    private static double vacTimeLimit;
    private Comuna comuna;
    private static double vacTimeCounter;
    private double x;//coordenadas del centro del cuadrado
    private double y;
    private static boolean active;
    private Rectangle edificio;
    private boolean alreadyBuild;
    public Vacunatorio(Comuna comuna){
        alreadyBuild = false;
        double lenght = SimulatorConfig.VAC_SIZE;
        this.semiLenght = lenght/2;//para posicionar el cuadrado trabajamos con el centro de este y ocupando la mitad del largo se nos hace mas facil encontrar los vertices
        this.vacTimeLimit= SimulatorConfig.VAC_TIME;
        this.vacTimeCounter = 0;
        this.comuna = comuna;
        this.x = (lenght/2)+ (comuna.getWidth()-(lenght/2))*Math.random();
        this.y = (lenght/2)+ (comuna.getHeight()-(lenght/2))*Math.random();
        this.active = false;
    }
    public boolean insideInNextState(Pedestrian individuo){
        if((individuo.getX_tPlusDelta()<=x+semiLenght) && (individuo.getX_tPlusDelta() >= x-semiLenght) && (individuo.getY_tPlusDelta() <= y+semiLenght) && (individuo.getY_tPlusDelta() >= y-semiLenght)){
            return true;
        }else{
            return false;
        }
    }//revisa si el individuo estará en su proximo estado dentro de un vacunatorio
    public void makeBuilding(){
        double lenght = SimulatorConfig.VAC_SIZE;
        edificio = new Rectangle(lenght,lenght, Color.LIGHTGREEN);
        edificio.setX(x-lenght/2);
        edificio.setY(y-lenght/2);
        edificio.setStroke(Color.DARKGREEN);
        edificio.setStyle("-fx-opacity: 0.5");
        comuna.getView().getChildren().add(edificio);
        alreadyBuild = true;
    }
    public boolean isAlreadyBuild(){return alreadyBuild;}
    public static void increaseVacTimeCounter(double delta_t){
        Vacunatorio.vacTimeCounter+=delta_t;
        if(vacTimeCounter>=vacTimeLimit){
            Vacunatorio.active = true;
        }
    }//aumenta el cronometro del timepo sin vacunatorios
    public static void computeNextState(double delta_t){
        if(!Vacunatorio.active){
            increaseVacTimeCounter(delta_t);
        }
    }//actualiza el booleano active en caso de ser necesario
    public static boolean isActive(){return Vacunatorio.active;}


}