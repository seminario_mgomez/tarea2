import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.Pane;
import java.util.ArrayList;

public class Comuna {
    private Rectangle2D territory;
    private ComunaView view;
    private Pane graph;
    public static XYChart.Series seriesI;
    public static XYChart.Series seriesS;
    public static XYChart.Series seriesR;
    public static XYChart.Series seriesV;
    private ArrayList<Pedestrian> poblacion;
    private ArrayList<Vacunatorio> vacunatorios;
    public Comuna(){
        poblacion = null;
        double width = SimulatorConfig.WIDTH;
        double length = SimulatorConfig.LENGTH;
        seriesR = new XYChart.Series();
        seriesI = new XYChart.Series();
        seriesS = new XYChart.Series();
        seriesV = new XYChart.Series();
        territory = new Rectangle2D(0,0, width, length);
        view = new ComunaView(this);
        graph = new Pane();
        poblacionBuilder();
        vacunatoriosBuilder();
        makeGraph();
    }
    public double getWidth() {
        return territory.getWidth();
    }
    public double getHeight() {
        return territory.getHeight();
    }
    private void poblacionBuilder(){
        poblacion = new ArrayList<Pedestrian>();
        double peopleWithMask = SimulatorConfig.M*SimulatorConfig.N;
        for(int i=0; i<SimulatorConfig.N; i++){
            if (i < SimulatorConfig.I && i < peopleWithMask) {//introduce personas infectadas con mascarillas
                poblacion.add(new Pedestrian(this, (Math.random()*0.2 + 0.9) * SimulatorConfig.SPEED, SimulatorConfig.DELTA_THETA,  SimulatorConfig.I_TIME,'i', true));
            } if (i < SimulatorConfig.I && i >= peopleWithMask) {//introduce personas infectadas sin mascarillas
                poblacion.add(new Pedestrian(this, (Math.random()*0.2 + 0.9) * SimulatorConfig.SPEED, SimulatorConfig.DELTA_THETA,  SimulatorConfig.I_TIME,'i', false));
            } if (i >= SimulatorConfig.I && i < peopleWithMask) {//introduce personas susceptibles con mascarillas
                poblacion.add(new Pedestrian(this, (Math.random()*0.2 + 0.9) * SimulatorConfig.SPEED, SimulatorConfig.DELTA_THETA,  SimulatorConfig.I_TIME,'s', true));
            } if (i >= SimulatorConfig.I && i >= peopleWithMask) {//introduce personas susceptibles sin mascarillas
                poblacion.add(new Pedestrian(this, (Math.random()*0.2 + 0.9) * SimulatorConfig.SPEED, SimulatorConfig.DELTA_THETA, SimulatorConfig.I_TIME,'s',  false));
            }
        }
        setPoblacion(poblacion);
    }
    private void vacunatoriosBuilder(){
        vacunatorios = new ArrayList<Vacunatorio>();
        for(int i=0; i<SimulatorConfig.NUM_VAC; i++){
            vacunatorios.add(new Vacunatorio(this));
        }
    }
    //Construye los vacunatorios en la interfaz gráfica
    private void showVacunatorios(){
        for (int k = 0; k < vacunatorios.size(); k++) {
            if (!vacunatorios.get(k).isAlreadyBuild()) {
                vacunatorios.get(k).makeBuilding();
            }
        }
    }
    public void vaccineLogic(int i){
        if(Vacunatorio.isActive()){
            showVacunatorios();
            if(poblacion.get(i).suceptible()) {
                for (int k = 0; k < vacunatorios.size(); k++) {
                    if (vacunatorios.get(k).insideInNextState(poblacion.get(i))) {
                        poblacion.get(i).vacunarse();
                        poblacion.get(i).view.changeShape('v');
                    }
                }
            }
        }
    }//corrobora si el individuo en su proxima posicion se encontrará dentro de un centro de vacunacion, en caso de ser asi, cambia su proximo estado a vacunado
    public void poblacionRestart(){
        restartGraph();
        clearPoblacion();
        poblacionBuilder();
        vacunatoriosBuilder();
    }
    public void computeNextState(){
        Vacunatorio.computeNextState(SimulatorConfig.DELTA_T);
        for (int i=0; i<poblacion.size(); i++ ){
            vaccineLogic(i);
            poblacion.get(i).computeNextState();
            for(int j=(i+1); j<poblacion.size(); j++){
                if(poblacion.get(i).contactoEstrecho(poblacion.get(j))){
                    Pedestrian.Exposicion(poblacion.get(j),poblacion.get(i));//para cada individuo se verifica si ha tenido contacto estrecho con cualquiera de los otros individuos
                }
            }

        }
    }
    public void updateState () {poblacion.forEach((Pedestrian) -> {Pedestrian.updateState();});}
    public void updateGraph(double simulationTime){
        int I = 0;
        int S = 0;
        int R = 0;
        int V = 0;
        for(int i=0; i<poblacion.size(); i++){
            I+= poblacion.get(i).infectado() ? 1:0;
            S+= poblacion.get(i).suceptible() ? 1:0;
            R+= poblacion.get(i).recuperado() ? 1:0;
            V+= poblacion.get(i).vacunado() ? 1:0;
        }
        Comuna.seriesS.getData().add(new XYChart.Data(simulationTime,S));
        Comuna.seriesI.getData().add(new XYChart.Data(simulationTime,I));
        Comuna.seriesR.getData().add(new XYChart.Data(simulationTime,R));
        Comuna.seriesV.getData().add(new XYChart.Data(simulationTime,V));
    }
    public void updateView(){view.update();}
    public void setPoblacion(ArrayList<Pedestrian> poblacion){this.poblacion = poblacion;}
    public void clearPoblacion(){
        view.restart();
        this.poblacion = null;
    }

    public ArrayList<Pedestrian> getPoblacion(){return poblacion;}
    public Group getView() {return view;}
    public Pane getGraph(){return graph;}
    public void makeGraph(){
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Tiempo de simulacion");
        yAxis.setLabel("Cantidad de individuos");
        final AreaChart<Number,Number> areaChart =
                new AreaChart<Number,Number>(xAxis,yAxis);
        areaChart.setTitle("Evolucion pandemia");
        seriesI.setName("Infectados");
        seriesS.setName("Susceptibles");
        seriesR.setName("Recuperados");
        seriesV.setName("Vacunados");
        areaChart.getData().addAll(seriesI,seriesS,seriesR,seriesV);
        graph.getChildren().add(areaChart);

    }
    public void restartGraph(){
        seriesS.getData().clear();
        seriesI.getData().clear();
        seriesR.getData().clear();
        seriesV.getData().clear();
    }
 }
