import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.input.KeyCode;
import javafx.scene.media.AudioClip;
import javafx.util.Duration;
import javafx.scene.input.KeyEvent;
import javafx.scene.control.ToggleButton;

public class Simulator {
    private Timeline animation;
    private Comuna comuna;
    private double simulationSamplingTime;
    private double simulationTime;  // it goes along with real time, faster or slower than real time
    private double delta_t;   // precision of discrete simulation time
    public static AudioClip beep;


    /**
     * @param framePerSecond frequency of new views on screen
     * @param simulationTime2realTimeRate how faster the simulation runs relative to real time
     */
    public Simulator (double framePerSecond, double simulationTime2realTimeRate, Comuna comuna){
        this.comuna = comuna;
        double viewRefreshPeriod = 1 / framePerSecond; // in [ms] real time used to display
        simulationSamplingTime = viewRefreshPeriod *simulationTime2realTimeRate;
        delta_t = SimulatorConfig.DELTA_T;
        simulationTime = 0;
        animation = new Timeline(new KeyFrame(Duration.millis(viewRefreshPeriod*1000), e->takeAction()));
        animation.setCycleCount(Timeline.INDEFINITE);
    }
    private void takeAction() {

        double nextStop=simulationTime+simulationSamplingTime;
        for(; simulationTime<nextStop; simulationTime+=delta_t) {
            comuna.computeNextState(); // compute its next state based on current global state
            comuna.updateGraph(simulationTime);
            comuna.updateState();            // update its state
            comuna.updateView();
        }}

    public void start(){
        simulationTime=0;
        comuna.poblacionRestart();
        animation.play();
        SimulatorMenuBar.setMenu.setDisable(true);
        comuna.getView().setOnKeyPressed( e->keyHandle(e));
    }
    private void keyHandle (KeyEvent e) {
        switch (e.getCode()){
            case RIGHT:
                speedup();
                break;
            case LEFT:
                slowdown();
                break;
        }
    }
    public void stop(){
        animation.stop();
        SimulatorMenuBar.setMenu.setDisable(false);
    }
    public void MenuParametros(){
        Stage stage = new Stage();
        stage.setTitle("Parameters");
        //Cajas
        VBox vbox = new VBox(5);
        HBox N_I_Box = new HBox(5);
        HBox Speed_Deltat_Deltatheta_ITimeBox = new HBox(5);
        HBox probabilidadesBox = new HBox(5);
        HBox boton_box = new HBox();
        HBox audio_box = new HBox();
        HBox slider_box = new HBox();
        //Formulario
        Slider slider = new Slider(0,1,0);
        Spinner<Integer> N = new Spinner<Integer>();
        SpinnerValueFactory<Integer> valueFactory_N = new SpinnerValueFactory.IntegerSpinnerValueFactory(1,100,1);
        N.setValueFactory(valueFactory_N);
        Spinner<Integer> I = new Spinner<Integer>();
        SpinnerValueFactory<Integer> valueFactory_I = new SpinnerValueFactory.IntegerSpinnerValueFactory(1,100,1);
        I.setValueFactory(valueFactory_I);
        TextField Speed = new TextField();
        TextField Delta_t = new TextField();
        TextField Delta_theta = new TextField();
        TextField P0 = new TextField();
        TextField P1 = new TextField();
        TextField P2 = new TextField();
        TextField I_time = new TextField();
        Button boton = new Button("Aplicar cambios");
        Speed.setPromptText("Speed");
        Delta_t.setPromptText("Delta_t");
        Delta_theta.setPromptText("Delta_theta");
        P0.setPromptText("P0");
        P1.setPromptText("P1");
        P2.setPromptText("P2");
        I_time.setPromptText("Tiempo de recuperacion");
        ToggleButton toggleAudio = new ToggleButton("Turn Off Audio");
        //Agregar eventos
        boton.setOnAction(e->{
            try {
                if (toggleAudio.isSelected()){beep.setVolume(0);}
                else{beep.setVolume(100);}

                SimulatorConfig.M = slider.getValue();
                SimulatorConfig.SPEED = Double.parseDouble(Speed.getText());
                SimulatorConfig.DELTA_T = Double.parseDouble(Delta_t.getText());
                SimulatorConfig.DELTA_THETA = Double.parseDouble(Delta_theta.getText());
                SimulatorConfig.P0 = Double.parseDouble(P0.getText());
                SimulatorConfig.P1 = Double.parseDouble(P1.getText());
                SimulatorConfig.P2 = Double.parseDouble(P2.getText());
                SimulatorConfig.I_TIME = Double.parseDouble(I_time.getText());
                SimulatorConfig.N = valueFactory_N.getValue().doubleValue();
                SimulatorConfig.I = valueFactory_I.getValue().doubleValue();
                this.start();
                stage.close();
            }catch (NumberFormatException error){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setTitle("Error");
                alert.setContentText("Parametros vacios");
                alert.showAndWait();
            }
        });
        //Configuracion de cajas
        probabilidadesBox.setAlignment(Pos.CENTER);
        slider_box.setAlignment(Pos.CENTER);
        vbox.setAlignment(Pos.CENTER);
        N_I_Box.setAlignment(Pos.CENTER);
        Speed_Deltat_Deltatheta_ITimeBox.setAlignment(Pos.CENTER);
        audio_box.setAlignment(Pos.CENTER);
        boton_box.setAlignment(Pos.CENTER);
        //Insertar formularios en cajas
        probabilidadesBox.getChildren().addAll(P0,P1,P2);
        slider_box.getChildren().addAll(new Label("set M: 0"),slider,new Label("1"));
        N_I_Box.getChildren().addAll(new Label("set N"),N,new Label("set I"),I);
        Speed_Deltat_Deltatheta_ITimeBox.getChildren().addAll(Speed,Delta_t,Delta_theta,I_time);
        audio_box.getChildren().add(toggleAudio);
        boton_box.getChildren().add(boton);
        vbox.getChildren().addAll(N_I_Box,Speed_Deltat_Deltatheta_ITimeBox, probabilidadesBox,slider_box ,audio_box,boton_box);
        //Construir escena
        stage.setScene(new Scene(vbox, 500, 500));
        stage.show();
    }
    public void speedup(){
        simulationSamplingTime *= 2;
    }
    public void slowdown(){
        simulationSamplingTime /= 2;
    }
}
